
# Formelsammlung für Algorithmsche Graphtheorie

Dieses Dokument wurde 2018 von Björn Jürgens erstellt auf Basis des Mitschriebes “Algorithmisch Graphentheorie und Perfekte Graphen” von Dr. rer. nat. Ignaz Rutter im Rahmen der Vorlesung "Algorithmisch Graphentheorie" von Torsten Ueckerdt.

Die Herren Rutter und Ueckerdt sind in keiner Weise verantwortlich für dieses Dokument.

Sie finden die aktuellste Version des Dokumentes [hier](https://gitlab.com/juergens/agt/builds/artifacts/master/file/build/agt_mitschrieb.pdf?job=build). 

# contributing

Das Dokument selbst ist 100% auf Deutsch, jedoch wird im Code und im gitlab Deutsch und Englisch bunt gemischt.

Das vermutlich niemals mehr als 1 Person gleichzeit an dem Dokument arbeitet wird, wird auf spezielle contribution-guidelines verzichtet.

## quickstart

	sudo apt install texlive-full evince pandoc pandoc-citeproc
	git clone --recurse-submodules git@gitlab.com:git@gitlab.com:juergens/agt.git
	cd agt
	make pdf
	envince build/example.pdf

If you want to add a section "conclusion", you add the file "conclusion.md" to "source" and then you add "conclusion.md" to `_SECTIONS.txt`

E.g if you want to add a section "conclusion", you add the file "conclusion.md" to "source" and then you add "conclusion.md" to `_SECTIONS.txt`

## Requirements

* pandoc > 2.0,
  * https://github.com/jgm/pandoc/releases
* latex: `sudo apt install texlive-full `
* csl in csl subfolder: `git submodule update --recursive --remote`


## structure of the text

Die Kapitel sind genau so wie im Script von 2016. Die Definitionen sind in der gleichen Reihenfolg wie im Script.

Die Zwischenüberschriften orientieren sich an den Unterkapiteln im Script.  Bei besonders kurzen Kapiteln habe ich die Zwischenüberschriften ausgelassen, und ich habe sie i. d. R. anders formuliert.

Die Definitionen sind weitestgehendst aus dem Skript übernommen. Manchmal habe ich sie etwas anders formuliert. Und manchmal habe ich eine ganz andere Definition verwendet (i.d.R. von wikipedia)

## Configuring the build system

The build-system is based on [this](https://gitlab.com/juergens/markdown-latex-boilerplate) boilerplate.

in config text file `config.txt`, you may have these default entries

    SECTIONS_FILEPATH=_SECTIONS.txt
    BUILDNAME=example
    REFERENCES=references.bib
    TEMPLATE=template.tex
    # TEMPLATE=ut-thesis.tex
    CSL=../elsevier-with-titles

This is what it means

	SECTIONS_FILEPATH=
		Holds a link to a file
		that contains a list
		of pages to join

	BUILDNAME=
		Let's you define the name of the generated file in the /build/ folder.

	REFERENCES=
		Links to pandoc reference file
		for the biography

	TEMPLATE=
		Links to template file use by LaTeX

	CSL=
		Choose from a list of Citation Style Language
		files found in ./CSL/ e.g. IEEE style.

Note:
 * `#` are comments.
 * You cannot have spaces in your key=value like `TEMPLATE = ut-thesis.tex`


### SECTIONS_FILEPATH

The file in SECTIONS_FILEPATH might look like this:

	example.md references.md

Where each file is entered in a single line delimitated by a single ` ` in sequence.

The reason is because the build file is automagically copying the content from `sections.txt` directly into `$(SECTIONS)` in for example the pandoc build html command line: `pandoc -S -5 ...etc... -t html --normalize $(SECTIONS)`. (e.g. `pandoc -S -5 ...etc... -t html --normalize example.md references.md`


## running the build

	cd agt && make

After this the compiled document can be found in folder `build`

For more build targets, see `Makefile`

## CI

CI should be set up and read to go. You can always find the most recent build [here](https://gitlab.com/juergens/agt/builds/artifacts/master/browse/build/?job=build).

if for some reason the CI-Runner stops working, you can set up a new one like this

get token from: Setting-->
CI / CD Settings --> Runners --> Specific Runners --> Setup a specific Runner manually

Configure Runner with token from above:

	docker run --rm -t -i -v /srv/gitlab-runner/config:/etc/gitlab-runner --name gitlab-runner gitlab/gitlab-runner register

start runner

	docker run -d --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest

view runner's logs

	docker logs gitlab-runner


## Tips and Tricks


### auto build and preview

	evince build/example.pdf & while inotifywait -e close_write source; do make pdf; done

What's happening here?

`evince` is a pdf reader, that automatically refreshes, when the document changes. `&` will push evince to the background. `inotifywait` is a program, that stops, when a file is changed, and `make pdf` rebuilds the pdf


### atom texteditor

plugins I like:

* `minimap`

### windows

[the upstream repository](https://github.com/davecap/markdown-latex-boilerplate) was focusing on windows-user and contains a lot of useful tipps
