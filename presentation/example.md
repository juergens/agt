---
author: Jonathan Dorian
title: my presentation
date: June 6, 2018
theme: simple
fragments: "false"
controlsTutorial: True
---

# chapter 1

## build

build with:

	make pres

run with

	firefox build/example.html

## dependencies

don't forget to install reveal&co with

	git submodule update --recursive --remote --init

# chapter 2

## subchapter 2a

## subchapter 2b

## subchapter 2c
