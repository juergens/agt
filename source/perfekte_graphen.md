
# Perfekte Graphen

\begin{deftable}

$G$ ist perfekt &&$\omega(G)=\chi(G), \alpha(G)=\kappa(G)$&
\\\hline Knotenverdopplung
\nt Ist das das richtige Wort? \idk
&$G \circ v$ \nt $v\in V$
&$(E',V')$
\newline mit $V'=V+v'$, $N(v')=N(v)$
\nt  $G=(V,E), v\in V$
& $v$ wird verdoppelt. Der neue Knoten $v'$ hat die selbe Nachbarschaft wie $v$
\\\hline Knotenmultiplikation
& $G \circ h$
\nt $h = (h_1,\ldots, h_n)$
\newline $h_i \in \mathbb{N}_0$
\newline $|h| = |V|$
& \multicolumn{2}{m{13cm}}{Jeder Knoten $x_i$ wird ersetzt durch unabh. Menge der Größe $h_i$, und jeder Knoten der Menge wird mit den Nachbarn von $x_i$ verbunden
}
\\\hline G ist $p$-kritisch &&$G$ ist nicht perfekt, aber jeder echte ind. Subgraph von $G$ ist perfekt& dies sind genau die Kreise ungerader länge und deren Komplemente (strong perfect graph conjecture)
\\\hline minimal imperfekt&&synonom zu $p$-kritisch&
\\\hline $\alpha, \omega$-partitionierbar
&
& $n = \alpha * \omega + 1$
\newline und $\forall x\in V: \alpha = \kappa(G-x), \omega = \chi (G-x)$
\newline mit $\alpha, \omega > 2$ beliebig in $\mathbb{N}$
& Jeder $p$-kritische Graph ist $\alpha, \omega$-partitionierbar mit $\alpha=\alpha(G)$ und $\omega=\omega(G)$
\\\hline partitionierbar && $\alpha, \omega$-partitionierbar \nt mit $\alpha=\alpha(G)$ und $\omega=\omega(G)$ & $\alpha$ und $\omega$ sind eindeutig

\end{deftable}
