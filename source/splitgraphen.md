
# Splitgraphen

\begin{deftable}
\tc Allgemeine Graph Eigenschaften
\\ & $V$& $G$ ist Vergleichbarkeitsgraph& nicht verwechseln mit der Knotenmenge $V$
\\\hline & $\overline V$& $\overline G$ ist Vergleichbarkeitsgraph&
\\\hline & $ C$&$G$ ist chordal& nicht verwechseln mit der Kreis $C_n$
\\\hline & $\overline C$& $\overline G$ ist chordal&

\\\hline \tc Charakterisierung
\\ Splitgraph
& $V$
& $\exists$ Zerlegung $ V=S+K: S$ unabh. Menge, $K$ vollständige Menge
& Die Kanten zwischen $S$ und $K$ können beliebig sein

\\\hline \tc Gradsequenz
\\ Gradsequenz \nt eines Graphen $G$
& $\Delta =[d_1,d_2,$ $\dots,d_n]$
\nt $d_i \in \mathbb{Z}, d_i < d_{i+1}$
& $\forall i: d(v_i) = d_i$
& Man schreibt die Grade der einzelnen Knoten in einer Reihe auf
\\\hline graphisch&&\mcd{Eine Folge $\Delta$ ist graphisch, wenn es einen ungerichteten Graphen mit dieser Gradsequenz gibt}
\\\hline Erdős-Gallai-Ungleichung (EGU)
&
& $\forall r \in \{1,2,\dots, n-1\}:$
\newline $$
\sum^r_{i=1}d_i \leq
r(r-1)+ \sum^n_{i=r+1} \min\{r,d_i\}
$$
& Folge ist genau dann graphisch wenn diese Ungleichung erfüllt ist \textbf{und} die Summe der Elemente gerade ist
\end{deftable}
