
# Permutationsgraphen

\begin{deftable}
\tc Allgemeines
\\ Permutation
& $\pi$
& $[\pi_1,\pi_2,$ $\dots,\pi_n]$
\nt mit $\pi_i =$ Bild$(\pi(i))$
& Folge von $n$ Zahlen von $1$ bis $n$ mit vertauschter Reihenfolge
\\\hline
& $\pi_i^{-1}$
& $(\pi^{-1})_i$
& Position von $\pi_i$ in Permutation
\\\hline Inversionsgraph
& $G[\pi]$
& Knoten: Nummern von 1 bis $n$
\newline Kante $\Leftrightarrow$ kleinere Zahl steht rechts von größerer Zahl
& Jede Kante entspricht einer Vertauschung in der Permutation
\\\hline Permutationsgraph
& $G[\pi]$ \nt wie Inversionsgraph
& $V = (1,2,$ $\dots,n)$
\newline $ij\in E \Leftrightarrow $
\newline $(i-j)(\pi_i^{-1}-\pi_j^{-1})<0$
& Ich habe noch nicht verstanden was der Unerschied zu Inversionsgraph ist \idc
\\\hline \tc Charakterisierung
\\ Satz 6.1 &&\mcd{Graph $G$ ist Permutationsgraph $\iif$ $G$ und $\hat G$ sind Vergleichbarkeitsgraphen}
\\\hline Permutationsbeschriftung &&\mcd{Knoten eines Graphen haben Werte von $0$ bis $n$. Eine Permutationsbeschriftung ist eine Permutation dieser Werte, sodass $G=G[\pi]$}
\\\hline \tc Anwendungen
\\ Matching-Repräsentation
&
& Obere Knoten: $1,\dots,n$
\newline untere Knoten: $\pi_1,\dots,\pi_n$
\newline Kanten zwischen gleichen Zahen
& Permutation wird als Matching dargestellt
\\\hline kanonische Sortierungsstrategie &&bei Sortierung von Queues wird jede neue Zahl in die erste Verfügbare Queue geschoben&
\\\hline kanonische Färbung &&Färbung, die durch Kanonische Sortierung entsteht, wenn jede Queue zu einer Farbe wird&
\end{deftable}
