
# Intervallgraphen

\begin{deftable}
\tc Allgemeines
\\ Intervallgraphen &&\textit{siehe Kapitel 1}&
\\\hline asteroidales Tripel &&\mcd{Tripel von Knoten, die sich \textbf{nicht} so anordnenen lassen, dass jeder Pfad vom ersten zum dritten Knoten die Nachbarschaft des zweiten durchläuft}
\\\hline Halbordnungs Nützlichkeitsfunktion&&\idc
\\\hline Semiordnung&&\idc
\\\hline Indifferenzgraphen &&& äquivalent zu Einheitsintervallgraphen
\end{deftable}
