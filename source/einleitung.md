# Einleitung

\begin{deftable}

\tc allgemeine Mathematik
\\ disjunkte Mengen
& $ $
& $A\cap B = \emptyset$
&
\\\hline
disjunkte Vereinigung
& $C = A + B$
& $C = A \cup B$ \newline mit $A\cap B = \emptyset$
& \\\hline
Potenzmenge
& $P(X)$ \newline oder $\mathcal P(X)$
\nt $X$ ist Menge
& $\{ U \mid U \subseteq X \}$ &
\\\hline (Binäre) Relation
& $R$
& $R: X \rightarrow P(X)$ \newline \tiny mit: $P(X)$ Potenzmenge von $X$
\newline \normalsize oder $R \subseteq X \times X$
\nt mit $(x,y) \in R$
&
\\\hline Äquivalenzrelation & & \mcd{Relation, die symmetrisch, reflexiv und transitiv ist}
\\\hline


\tc Graphtheorie
\\Graph
&$G=(V,E)$
& $V$ Knotenmenge (vertex set)
\newline $E \subseteq V\times V$ Kantenmenge (edge set)
& $E$ ist eine binäre, irreflexive Relation auf $V$. $V$ ist beliebige Menge.
\\\hline Ungerichteter Graph &&$G=(V,E)$\nt mit $E=E^{-1}$&
\\\hline Adjazente Knoten&$Adj(u)$&$\{v|\exists (u,v)\in E\}$& Knoten, mit Kante von $u$
\\\hline Nachbarschaft &$N(v)$&$\{v\}+Adj(v)$&
\\\hline Umkehrgraph&$G^{-1}$&$(V,E^{-1})$
\nt $E^{-1}=\{(v,u)|(u,v)\in E\}$ \nt $G=(V,E)$
& alle Kanten zeigen in die andere Richtung
\\\hline symmetrischer Abschluss
&$\hat G$
&$(V,\hat E)$ \nt mit $\hat E=E\cup E^{-1}$
& gerichtete Kanten "vergessen" ihre Richtung
\\\hline Orientierter Graph &$H$$(V,F)$\nt mit $F$ antisymmetrisch (d.h. $F\cap F^{-1}=\varnothing$)& ungerichtete Kanten bekommen eine Richtung
\\\hline Orientierung \nt von $G$ &$H$ (oder $F$)&$F+F^{-1}=E$ \nt mit $H=(V,F)$,$G=(V,E)$, G ungerichtet& Man bezeichnet sowohl den Graphen $H$ als auch die Kantenmenge $F$ als Orientierung von $G$ bzw. $E$
\\\hline Komplement \nt eines ungerichteten Graphen&$\overline G$
&$(V,\overline E)$
\nt mit $\overline E=\{(u,v)\in V\times V|u \neq v, (u,v)\notin E\}$
& Kanten von $\overline G$ sind da, wo bei $G$ keine Kanten sind
\\\hline vollständiger Graph&$K_n$&$|V| = n\newline E=V\times V$& alle Knoten sind mit allen anderen Knoten verbunden
\\\hline Subgraph&$H=(V', E') $&$V'\subseteq V \newline E'\subseteq E$&
\\\hline induzierter Subgraph
&$G_A=(A,E_A)$
&$E_A=\{(u,v)|u,v\in A\}$
\newline $A \subseteq V$
& Subgraph, bei dem Knoten gegeben sind, und alle Kanten zwischen diesen Knoten mitgenommen werden
\\\hline induzierter Subgraph (über Kanten)
&$G_A=(V_A,A)$
&$V_A=\{u |(u,v)$ oder $(v,u) \in A\}$
\newline $A \subseteq E$
& Subgraph, bei dem Kanten gegeben sind, und die entsprechenden Knoten mitgenommen werden
\\\hline Clique &$ G_A \tilde = K_r$&$ $& Subgraph, der vollständig verbunden ist
\\\hline (in\-klus\-ions-) max\-ima\-le Cli\-que&$ $&$ $& Clique, die kein echter Subgraph einer anderen Clique ist
\\\hline kar\-dinalitäts\-max\-imale Clique&$ $&$ $& größte Clique
\\\hline


\tc Maßzahlen
\\ Cliquenzahl&$\omega(G) $&$ $& Große der größsten Clique in $G$
\\\hline Partition
&$ $
&$X= A_1+\dots+A_n$
\nt mit $X, A_i$ Mengen
& Zerlegung einer Menge in disjunkte Teilmengen
\\\hline Cliquen\-über\-deck\-ung&$ $
& $G = A_1 + \dots + A_n$ \newline mit: $A_i$ ist Clique
& Partition von $G$ in $A_i$, wobei jedes $A_i$ Clique it
\\\hline Cliquen\-über\-deck\-ungs\-zahl&$\kappa(G)$
&Größe der kleinsten Cliquenüberdeckung
&In wieviele Cliquen lässt sich $G$ zerlegen?
\\\hline un\-ab\-häng\-ige Menge &$ $
&$X \subseteq V$ \newline mit $\forall u,v \in X \nexists uv\in E $
& Knoten, die paarweise nicht adjazent sind
\\\hline Un\-ab\-häng\-ig\-keits\-zahl &$\alpha(G)$&$ $&Größe der größten unabhängigen Menge in $G$
\\\hline echte $c$-Färbung&$ $&Partition $V=A_1+\dots+A_c$, sodass $X_i$ unabh. Menge& Keine Knoten der gleichen Farbe sind benachbart
\\\hline unechte $c$-Färbung&$ $& Wie echte Färbung, nur sind gleichfarbige Nacbarn erlaubt & ist das gleiche wie eine Partition (\idk)
\\\hline chromatische Zahl &$\chi(G)$&$\min\{c \mid \exists c \text{-Färbung von }G\}$& mindestanzahl an Farben, die man für eine echte Färbung des Graphen braucht
\\\hline


\tc Knoteneigenschaften
\\ Ausgangsgrad &$d^+(u)$&$|Adj(x)|$&
\\\hline Eingangsgrad &$d^-(u)$&$|\{<\in V \mid x\in Adj(y)\}| $&
\\\hline Quelle &$ $&Knoten mit Eingangsgrad 0&
\\\hline Senke &$ $&Knoten mit Ausgangsgrad 0&
\\\hline

\tc wichtige Subgraphen
\\ Pfad
&$ $
&$[v_0,\dots,v_n]$ \nt mit: $v_{i-1}v_i\in E$
& Folge von Knoten, bei der aufeinanderfolgende Elemente mit Kante verbunden sind.
\\&\mct{\vspace{-1em}\nt Anm.: "(einfacher) Pfad" und "Weg" wird hier anders benutzt als z.B. in "Graph Theory" (von Prof. Maria Axenovich)}
\\\hline a-b-Pfad&$P_{a,b}$&$[a,v_1,\dots,v_{n-1},b]$\nt mit: $v_{i-1}v_i\in E$
& Pfad, der mit $a$ beginnt und mit $b$ endet
\\\hline einfacher Pfad&$ $
& $[v_0,\dots,v_n]$ \nt mit: $v_{i-1}v_i\in E$
\newline $v_i \neq v_j \forall i\neq j $
& Pfad, wo kein Knoten wiederholt wird
\\\hline A-B-Pfad&$ $&a-b-Pfad mit $a\in A, b\in B$&Pfad, der irgendwo in $A$ beginnt und irgendwo in $B$ endet
\\\hline zu\-sam\-men\-häng\-en\-der Graph &$ $&$\forall u,v: \exists P_{u,v}$&
\\\hline Kreis (\textit{circle}) &$C_r$
&$[v_0,\dots,v_r,v_0]$ \nt mit: $v_{i-1}v_i\in E$
& Pfad, wo Anfang und ende gleich sind
\\\hline einfacher Kreis&$ $& analog zu \textit{einfacher Pfad}& Kreis, wo kein Knoten wiederholt wird.
\\\hline Sehne &$ $&$v_iv_j\in E$ mit $(|i-j|\mod n)>1$ ($n$ ist Länge des Kreises) & zusätzliche Kante im Kreis
\\\hline bipartiter Graph &$ $
&$\exists A,B: A+B=V$
\nt $A,B$ unabhängige Mengen
& Graph, der aus 2 unabhängige Mengen besteht. Wobei zischen den Mengen sind beliebig viele Kanten sein können.
\\\hline Vollständiger Graph&$K_n$&$|V|=n, E=V\times V$&
\\\hline Kreis ohne Sehnen&$C_n$
&$|V|=\{v_0,\dots,v_n\} (\text{mit }v_0=v_n),$
\newline $E=\{v_iv_{i+1}|0\leq i < n\}$
& \textit{engl.} "$t$-hole" (, wobei $t:=n$)
\\\hline Pfad ohne Sehnen&$P_n$&analog zu \textit{Kreise ohne Sehnen}&


\\\hline \tc allgemeine Graphklassen
\\ vollständiger bipartiter Graph &$K_{n,m}$
&$V = A + B$
\nt $|A|=n, |B|=m$
\newline $A,B$ unabhängige Mengen
\newline $E=\{ab|a\in A, b\in B\} \cup \{ba|a\in A, b\in B\}$
&bipartiter Graph, mit allen möglichen Kanten
\\\hline Sterngraph &$K_{1,n}$&&
\\\hline disjunkte Kopien von $K_n$&$mK_n$&&


\\\hline \tc spezielle Graphklassen
\\ Schnittgraph
&
&$V:$ Mengen aus $F$
\newline $E: uv\in E \iif \exists x: x \in u, x\in v$
\nt mit $F$ Familie von nichtleeren Mengen
& Es gibt eine Kante, wenn die beiden Mengen ein (oder mehr) gemeinesame Element haben
\nt Müssen Knoten nicht-leer sein? \idk
\\\hline Intervall\-graph &&\mcd{ Schnittgraph, wo  die Mengen Intervalle sind}
\\\hline echter Inter\-vall\-graph &&\mcd{ Wie Intervall\-graph, nur darf kein Intervall ein anderes umschließen}
\\\hline Kreisbogen\-graph&&\mcd{ Wie Intervallgraph, nur sind die Enden des Raumes miteinander Verbunden}
\\\hline Permutations\-graph&&\mcd{Schnittgraph von Strecken, deren Enden auf 2 parellel Geraden liegen \nt bessere Definition im Kapitel "Permutationsgraphen"}
\\\hline Kreissehnen\-graph &&\mcd{Permutations\-graph, nur liegen den Endpunkte auf einem Kreis}


\\\hline \tc spezielle Eigenschaften
\\hereditäre Eigenschaft&&Eigenschaft, für die gilt: $G$ hat diese Eigenschaft $\implies$ alle Subgraphen von $G$ haben diese Eigenschaft & "hereditär" = "erblich"
\\\hline chordaler Graph&&jeder Kreis (im Graphen) mit Länge $>3$ hat Sehne&
\\\hline transitiv orientierbar&&$\exists \text{Orientierung }F: \forall a,b,c\in V: ab\in F \land bc\in F \implies ac\in F$&
\\\hline Ver\-gleich\-bar\-keits\-graph && ungerichteter Graph, der transitiv orientierbar ist
\\\hline $\chi$-perfekt&&$\forall \text{ind.Subgraph } G_A \text{ von } G: \chi(G_A)=\omega(G_A) $&
\\\hline $\alpha$-perfekt&&$\forall  \text{ind.Subgraph }G_A \text{ von } G: \alpha(G_A)=\kappa(G_A) $&

\end{deftable}
