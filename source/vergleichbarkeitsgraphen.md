
# Vergleichbarkeitsgraphen

\begin{deftable}
\tc Allgemeines \\
Vergleichbarkeitsgraph &&\mcd{\textit{siehe Kap. 1}}
\\\hline  $\Gamma$-Relation
& $a b \Gamma a' b'$
\nt $ab, a'b' \in E$
& $\begin{cases}
a = a'\text{ und }bb' \notin E \\
b = b'\text{ und }aa' \notin E \\
\end{cases}$
& "Kante $ab$ erzwingt Kante $a'b'$". Zwei gerichtete Kanten haben ein gemeinsamen Anfang (oder gemeinsames Ende), aber es gibt keine dritte Kante, zwischen den beiden Enden (oder Anfängen)
\\\hline  $\Gamma$-Kette
& $a_0 b_0 \Gamma^* a_k b_k$
& $\exists a_ib_i: a_0b_0 \Gamma a_1b_1 \Gamma \dots \Gamma a_kb_k $
\nt $k \geq 0$, $a_ib_i\in E$
& Reflexive Transitive Hülle von $\Gamma$
\\\hline Implikationsklasse &&Äquivalenzklasse mit $\Gamma^*$ als Äquivalenzrelation&
\\\hline Menge der Implikationsklassen & $\mathcal I(G)$&Nomen est omen&
\\\hline Farbklasse & $\hat{\mathcal I}(G)$
& $\{ \hat A \mid A \in \mathcal I(G)\}$
& ungerichtete Implikationsklassen
\\\hline Dreieckslemma &&\mcd{Lemma über Implikationsklassen und benachbarte Knoten}
\\\hline

\tc TRO-Algorithmus

\\G-Zerlegung \nt von Graph $G$
& $E = \hat B_1 + \hat B_2 +\dots + \hat B_k$
\nt $E$ ist Kantenmenge eines Graphen, $G$ steht nicht für den Graph $G$
&  $\forall i \leq k: \hat B_i $ ist Implikationsklasse von $\hat B_i+ \dots + \hat B_k$
&
\\\hline Zerlegungsschema \nt von Graph $G$
& $[x_1y_1,$ $x_2y_2,$ $\dots,$ $x_ky_k]$
& $\exists G$-Zerlegung $ E= \hat B_1 + \hat B_2 + \dots + \hat B_k: \forall i \leq k: x_iy_i \in B_i$
& Das Zerlegungsschema gibt an, welche Kanten man entfernen muss, um die $G$-Zerlegung zu konstruieren
\\\hline Simplex \nt mit Rang $r$
&
& Clique $(V_S , S)$ mit:
\newline jede ungerichtet Kante $ \hat{ab} \in S$ gehört zu einer anderen Farbklasse
 \nt und $|V_S| = r+1$
&
\\\hline Multiplex \nt mit Rang $r$
& $(V_M,M)$
& $M=\{ab\in E \mid \exists xy\in S: ab\Gamma^*xy \}$
\nt $S$ ist Simplex mit Rang $r$
& Alle Knoten, die zu Farbklassen des Simplex' gehören
\\\hline TRO Theorem &&\mcd{Satz über Zusammenhang zwischen Implikationsklassen,  Vergleichbarkeitsgraphen und G-Zerlegungen}
\\\hline TRO-Algorithmus&&\mcd{linearzeit Algorithmus, der Implikationsklassen findet. Und ich glaube die Version aus der Vorlesung findet auch $\alpha$, $\omega$, $\kappa$ und $\chi$ \idk}
\\\hline Satz von Dilworth &&\mcd{Satz über Anzahl von $\Gamma$-Ketten und Antiketten }
\\\hline Antikette &&Menge von Elementen, die paarweise nicht in $\Gamma$-Relation stehen&
\\\hline
\tc partieller Ordnungen
\\ Partielle Ordung
&$(\mathbf X,\mathbf P)$
\nt $\mathbf X$ Menge, $\mathbf P$ Relation
& $\mathbf P$ ist relfexiv, antisymmetrisch und transitiv
& engl "poset" (\textbf{p}artially \textbf{o}rdered \textbf{s}et)
\\\hline Topologische Sortierung
\nt von einer Menge $M$ für eine Relation $R$
&
& eine  strenge Totalordnung, die $R$ einschließt
&
\\\hline Lineare Erweiterung&&\mcd{\idc}
\\\hline Realisierer von $\mathbf P$ &&\mcd{\idc}
\end{deftable}
