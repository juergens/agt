
# Chordale Graphen

\begin{deftable}
chordal&& \mcd{ jeder Kreis der Länge größer als 3 hat eine Sehne}
\\\hline simplizial && $Adj(x)$ ist Clique&
\\\hline perfektes Eliminationsschema (PES)
&Ordnung $\sigma$ ist PES
& $\forall i \in [1,\dots,n]: v_i$ ist simplizial in $G_{[v_i, \dots, v_n]}$
\nt $\sigma = [v_1, \dots, v_n]$
& Wenn man die Knoten in der Reihenfolge $\sigma$ entfernt, ist jeder Knoten simplizial im verbleibenden Graphen
\\\hline $a$-$b$-Separator
& $S$ ist $a$-$b$-Separator
\nt $S \subseteq V$, $a,b \in V$, $a,b$ nicht adjazent
& $a$ und $b$ liegen in verschiedenen Zusammenhangskomponenten von $G-S$
&
\\\hline (Knoten-) Separator && $a$-$b$-Separator mit beliebigem $a,b$ &
\\\hline LexBFS &&Algo. zur Erkennung Chordaler Graphen&
\\\hline Helly-Eigenschaft
& Familie $\{T_i\}_{i\in I}$ von Teilmengen einer Menge $T$ erfüllt Helly-Eigenschaft
& $\forall$ Teilmengen $J \subseteq I: \forall i,j \in J: T_i \cap T_j \neq \emptyset $
\newline impliziert $\cap_{j\in J} T_j \neq \emptyset$
& Famile erfüllt Helly-Eigenschaft $\Leftrightarrow$ Der Schnitt einer Unterfamilie leer ist, dann sind auch alle paarweisen Schnitte der Mengen der Unterfamilie leer 
\\\hline Zusammenhangskomponente & & \mcd{Menge von Knoten, die paarweise durch Pfade verbunden sind}
\\\hline &&&
\end{deftable}
