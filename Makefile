
# Load in new config settings
include _CONFIG.txt

# This combines all the filepaths in SECTIONS_FILEPATH file
SECTIONS := $(shell cat $(SECTIONS_FILEPATH) | tr '\n\r' ' ' | tr '\n' ' ' )

TEMPLATE_DIR=$(shell realpath $(shell dirname $(TEMPLATE)))

TEX_CMD=TEXINPUTS=$(TEXINPUTS):$(TEMPLATE_DIR) pandoc --toc --bibliography=$(REFERENCES)  --csl=../csl/$(CSL).csl --template=../$(TEMPLATE) --top-level-division=chapter

.PHONY: all clean html pdf
default: pdf
.DEFAULT_GOAL := pdf

all: pdf tex


pre:
	mkdir -p build

clean:
	rm -rf build


# build the document
pdf: pre
		cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).pdf $(SECTIONS)

# only build tex (good for migrating to tex-only build system)
tex: pre
		cd ./source/  && \
		$(TEX_CMD) -o ../build/$(BUILDNAME).tex $(SECTIONS)
